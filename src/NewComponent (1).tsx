import React from "react";
import Stack from "usr/layout/Stack.comp";
import Typography from "usr/data-display/Typography.comp";
import GridWithCells from "usr/layout/GridWithCells.comp";
import TextField from "usr/inputs/TextField.comp";
import DateTimePicker from "usr/inputs/DateTimePicker.comp";
import Checkbox from "usr/inputs/Checkbox.comp";
import RadioGroupWithItems from "usr/inputs/RadioGroupWithItems.comp";
import Button from "usr/inputs/Button.comp";

export interface NewComponentProps {}

function NewComponent(props: NewComponentProps): JSX.Element {
  return (
    <Stack
      cells={[
        <div
          key="item1"
          style={{
            width: "100%",
            minHeight: "2em",
            height: "100%",
            borderRadius: "4px",
            border: "1px dashed #dddddd"
          }}
        />,
        <Typography
          key="item2"
          align="inherit"
          color="initial"
          display="initial"
          gutterBottom={false}
          noWrap={false}
          paragraph={false}
          text="GRANT QUERIES WITH DATES"
          variant="h1"
        />,
        <div
          key="item3"
          style={{
            width: "100%",
            minHeight: "2em",
            height: "100%",
            borderRadius: "4px",
            border: "1px dashed #dddddd"
          }}
        />,
        <Stack
          key="item4"
          cells={[
            <GridWithCells
              key="item5"
              alignContent="stretch"
              alignItems="stretch"
              cells={[
                {
                  child: (
                    <div
                      style={{
                        width: "100%",
                        minHeight: "2em",
                        height: "100%",
                        borderRadius: "4px",
                        border: "1px dashed #dddddd"
                      }}
                    />
                  ),
                  lg: "false",
                  md: "false",
                  sm: "false",
                  xl: "false",
                  xs: "auto",
                  zeroMinWidth: false
                },
                {
                  child: (
                    <Stack
                      cells={[
                        <Typography
                          key="item6"
                          align="inherit"
                          color="initial"
                          display="initial"
                          gutterBottom={false}
                          noWrap={false}
                          paragraph={false}
                          text="STATES"
                          variant="h3"
                        />,
                        <TextField
                          key="item7"
                          debounceDelay={300}
                          formControl={{ color: "primary", margin: "none" }}
                          helperText="STATES HERE"
                          inputControl={{ type: "text" }}
                          label="STATES"
                          multilineControl={{ multiline: false }}
                        />,
                        <Typography
                          key="item8"
                          align="inherit"
                          color="initial"
                          display="initial"
                          gutterBottom={false}
                          noWrap={false}
                          paragraph={false}
                          text="TYPED STATES WILL SHOW HERE{}"
                          variant="body1"
                        />,
                        <div
                          key="item9"
                          style={{
                            width: "100%",
                            minHeight: "2em",
                            height: "100%",
                            borderRadius: "4px",
                            border: "1px dashed #dddddd"
                          }}
                        />,
                        <Typography
                          key="item10"
                          align="inherit"
                          color="initial"
                          display="initial"
                          gutterBottom={false}
                          noWrap={false}
                          paragraph={false}
                          text="ZIP_CODES"
                          variant="body1"
                        />,
                        <TextField
                          key="item11"
                          debounceDelay={300}
                          formControl={{ margin: "none" }}
                          helperText="ZIP CODES HERE"
                          inputControl={{ type: "text" }}
                          label="ZIP CODES"
                        />,
                        <Typography
                          key="item12"
                          align="inherit"
                          color="initial"
                          display="initial"
                          gutterBottom={false}
                          noWrap={false}
                          paragraph={false}
                          text="STATES ZIPCODE {}"
                          variant="body1"
                        />,
                        <div
                          key="item13"
                          style={{
                            width: "100%",
                            minHeight: "2em",
                            height: "100%",
                            borderRadius: "4px",
                            border: "1px dashed #dddddd"
                          }}
                        />
                      ]}
                      direction="column"
                      justifyItems="center"
                      spacing="1"
                    />
                  ),
                  lg: "false",
                  md: "false",
                  sm: "false",
                  xl: "false",
                  xs: "auto",
                  zeroMinWidth: false
                },
                {
                  child: (
                    <Stack
                      cells={[
                        <Typography
                          key="item14"
                          align="inherit"
                          color="initial"
                          display="initial"
                          gutterBottom={false}
                          noWrap={false}
                          paragraph={false}
                          text="BETWEEN DATES"
                          variant="h3"
                        />,
                        <Typography
                          key="item15"
                          align="inherit"
                          color="initial"
                          display="initial"
                          gutterBottom={false}
                          noWrap={false}
                          paragraph={false}
                          text="FROM DATE"
                          variant="body1"
                        />,
                        <DateTimePicker
                          key="item16"
                          allowTextInput={true}
                          variant="dialog"
                        />,
                        <div
                          key="item17"
                          style={{
                            width: "100%",
                            minHeight: "2em",
                            height: "100%",
                            borderRadius: "4px",
                            border: "1px dashed #dddddd"
                          }}
                        />,
                        <Typography
                          key="item18"
                          align="inherit"
                          color="initial"
                          display="initial"
                          gutterBottom={false}
                          noWrap={false}
                          paragraph={false}
                          text="END_DATES"
                          variant="body1"
                        />,
                        <DateTimePicker
                          key="item19"
                          allowTextInput={true}
                          variant="dialog"
                        />,
                        <div
                          key="item20"
                          style={{
                            width: "100%",
                            minHeight: "2em",
                            height: "100%",
                            borderRadius: "4px",
                            border: "1px dashed #dddddd"
                          }}
                        />
                      ]}
                      direction="column"
                      justifyItems="center"
                      spacing="1"
                    />
                  ),
                  lg: "false",
                  md: "false",
                  sm: "false",
                  xl: "false",
                  xs: "auto",
                  zeroMinWidth: false
                },
                {
                  child: (
                    <Stack
                      cells={[
                        <Typography
                          key="item21"
                          align="inherit"
                          color="initial"
                          display="initial"
                          gutterBottom={false}
                          noWrap={false}
                          paragraph={false}
                          text="ORDER BY"
                          variant="h3"
                        />,
                        <GridWithCells
                          key="item22"
                          alignContent="stretch"
                          alignItems="stretch"
                          cells={[
                            {
                              child: (
                                <Checkbox
                                  color="default"
                                  label="BY CITY"
                                  labelPlacement="end"
                                />
                              ),
                              lg: "false",
                              md: "false",
                              sm: "false",
                              xl: "false",
                              xs: "auto",
                              zeroMinWidth: false
                            },
                            {
                              child: (
                                <RadioGroupWithItems
                                  items={[
                                    {
                                      color: "default",
                                      label: "ASCENDING",
                                      value: "asc"
                                    },
                                    {
                                      color: "default",
                                      label: "DECENDING",
                                      value: "des"
                                    }
                                  ]}
                                  row={true}
                                  selectedValue="asc"
                                  size="small"
                                />
                              ),
                              lg: "false",
                              md: "false",
                              sm: "false",
                              xl: "false",
                              xs: "auto",
                              zeroMinWidth: false
                            }
                          ]}
                          direction="row"
                          justify="flex-start"
                          spacing="1"
                          wrap="nowrap"
                        />,
                        <GridWithCells
                          key="item23"
                          alignContent="stretch"
                          alignItems="stretch"
                          cells={[
                            {
                              child: <Checkbox color="default" label="DATE" />,
                              lg: "false",
                              md: "false",
                              sm: "false",
                              xl: "false",
                              xs: "auto",
                              zeroMinWidth: false
                            },
                            {
                              child: (
                                <RadioGroupWithItems
                                  items={[
                                    {
                                      color: "default",
                                      label: "ASCENDING",
                                      value: "asc"
                                    },
                                    {
                                      color: "default",
                                      label: "DESCENDING",
                                      value: "des"
                                    }
                                  ]}
                                  row={true}
                                  selectedValue="asc"
                                  size="small"
                                />
                              ),
                              lg: "false",
                              md: "false",
                              sm: "false",
                              xl: "false",
                              xs: "auto",
                              zeroMinWidth: false
                            }
                          ]}
                          direction="row"
                          justify="flex-start"
                          spacing="1"
                          wrap="nowrap"
                        />,
                        <div
                          key="item24"
                          style={{
                            width: "100%",
                            minHeight: "2em",
                            height: "100%",
                            borderRadius: "4px",
                            border: "1px dashed #dddddd"
                          }}
                        />,
                        <div
                          key="item25"
                          style={{
                            width: "100%",
                            minHeight: "2em",
                            height: "100%",
                            borderRadius: "4px",
                            border: "1px dashed #dddddd"
                          }}
                        />,
                        <div
                          key="item26"
                          style={{
                            width: "100%",
                            minHeight: "2em",
                            height: "100%",
                            borderRadius: "4px",
                            border: "1px dashed #dddddd"
                          }}
                        />
                      ]}
                      direction="column"
                      justifyItems="center"
                      spacing="1"
                    />
                  ),
                  lg: "false",
                  md: "false",
                  sm: "false",
                  xl: "false",
                  xs: "auto",
                  zeroMinWidth: false
                },
                {
                  child: (
                    <div
                      style={{
                        width: "100%",
                        minHeight: "2em",
                        height: "100%",
                        borderRadius: "4px",
                        border: "1px dashed #dddddd"
                      }}
                    />
                  ),
                  lg: "false",
                  md: "false",
                  sm: "false",
                  xl: "false",
                  xs: "auto",
                  zeroMinWidth: false
                }
              ]}
              direction="row"
              justify="center"
              spacing="2"
              wrap="nowrap"
            />,
            <Button
              key="item27"
              fullWidth={true}
              label="Button"
              variant="contained"
            />,
            <div
              key="item28"
              style={{
                width: "100%",
                minHeight: "2em",
                height: "100%",
                borderRadius: "4px",
                border: "1px dashed #dddddd"
              }}
            />
          ]}
          direction="column"
          spacing="1"
        />,
        <div
          key="item29"
          style={{
            width: "100%",
            minHeight: "2em",
            height: "100%",
            borderRadius: "4px",
            border: "1px dashed #dddddd"
          }}
        />,
        <Stack
          key="item30"
          cells={[
            <GridWithCells
              key="item31"
              alignContent="stretch"
              alignItems="stretch"
              cells={[
                {
                  child: (
                    <div
                      style={{
                        width: "100%",
                        minHeight: "2em",
                        height: "100%",
                        borderRadius: "4px",
                        border: "1px dashed #dddddd"
                      }}
                    />
                  ),
                  lg: "false",
                  md: "false",
                  sm: "false",
                  xl: "false",
                  xs: "auto",
                  zeroMinWidth: false
                },
                {
                  child: (
                    <div
                      style={{
                        width: "100%",
                        minHeight: "2em",
                        height: "100%",
                        borderRadius: "4px",
                        border: "1px dashed #dddddd"
                      }}
                    />
                  ),
                  lg: "false",
                  md: "false",
                  sm: "false",
                  xl: "false",
                  xs: "auto",
                  zeroMinWidth: false
                },
                {
                  child: (
                    <div
                      style={{
                        width: "100%",
                        minHeight: "2em",
                        height: "100%",
                        borderRadius: "4px",
                        border: "1px dashed #dddddd"
                      }}
                    />
                  ),
                  lg: "false",
                  md: "false",
                  sm: "false",
                  xl: "false",
                  xs: "auto",
                  zeroMinWidth: false
                },
                {
                  child: (
                    <div
                      style={{
                        width: "100%",
                        minHeight: "2em",
                        height: "100%",
                        borderRadius: "4px",
                        border: "1px dashed #dddddd"
                      }}
                    />
                  ),
                  lg: "false",
                  md: "false",
                  sm: "false",
                  xl: "false",
                  xs: "auto",
                  zeroMinWidth: false
                },
                {
                  child: (
                    <div
                      style={{
                        width: "100%",
                        minHeight: "2em",
                        height: "100%",
                        borderRadius: "4px",
                        border: "1px dashed #dddddd"
                      }}
                    />
                  ),
                  lg: "false",
                  md: "false",
                  sm: "false",
                  xl: "false",
                  xs: "auto",
                  zeroMinWidth: false
                }
              ]}
              direction="row"
              justify="flex-start"
              spacing="0"
              wrap="nowrap"
            />,
            <Stack
              key="item32"
              cells={[
                <Typography
                  key="item33"
                  align="inherit"
                  color="initial"
                  display="initial"
                  gutterBottom={false}
                  noWrap={false}
                  paragraph={false}
                  text="FROM QUERY"
                  variant="body1"
                />,
                <GridWithCells
                  key="item34"
                  alignContent="stretch"
                  alignItems="stretch"
                  cells={[
                    {
                      child: (
                        <div
                          style={{
                            width: "100%",
                            minHeight: "2em",
                            height: "100%",
                            borderRadius: "4px",
                            border: "1px dashed #dddddd"
                          }}
                        />
                      ),
                      lg: "false",
                      md: "false",
                      sm: "false",
                      xl: "false",
                      xs: "auto",
                      zeroMinWidth: false
                    },
                    {
                      child: (
                        <Typography
                          align="inherit"
                          color="initial"
                          display="initial"
                          gutterBottom={false}
                          noWrap={true}
                          paragraph={false}
                          text="DOT NUMBER"
                          variant="body1"
                        />
                      ),
                      lg: "false",
                      md: "false",
                      sm: "false",
                      xl: "false",
                      xs: "auto",
                      zeroMinWidth: false
                    },
                    {
                      child: (
                        <Typography
                          align="inherit"
                          color="initial"
                          display="initial"
                          gutterBottom={false}
                          noWrap={true}
                          paragraph={false}
                          text="MC NUMBER"
                          variant="body1"
                        />
                      ),
                      lg: "false",
                      md: "false",
                      sm: "false",
                      xl: "false",
                      xs: "auto",
                      zeroMinWidth: false
                    },
                    {
                      child: (
                        <Typography
                          align="inherit"
                          color="initial"
                          display="initial"
                          gutterBottom={false}
                          noWrap={false}
                          paragraph={false}
                          text="LEGAL NAME"
                          variant="body1"
                        />
                      ),
                      lg: "false",
                      md: "false",
                      sm: "false",
                      xl: "false",
                      xs: "auto",
                      zeroMinWidth: false
                    },
                    {
                      child: (
                        <Typography
                          align="inherit"
                          color="initial"
                          display="initial"
                          gutterBottom={false}
                          noWrap={false}
                          paragraph={false}
                          text="CITY"
                          variant="body1"
                        />
                      ),
                      lg: "false",
                      md: "false",
                      sm: "false",
                      xl: "false",
                      xs: "auto",
                      zeroMinWidth: false
                    },
                    {
                      child: (
                        <Typography
                          align="inherit"
                          color="initial"
                          display="initial"
                          gutterBottom={false}
                          noWrap={true}
                          paragraph={false}
                          text="STATE"
                          variant="body1"
                        />
                      ),
                      lg: "false",
                      md: "false",
                      sm: "false",
                      xl: "false",
                      xs: "auto",
                      zeroMinWidth: false
                    },
                    {
                      child: (
                        <Typography
                          align="inherit"
                          color="initial"
                          display="initial"
                          gutterBottom={false}
                          noWrap={true}
                          paragraph={false}
                          text="MC_ADD_DATE"
                          variant="body1"
                        />
                      ),
                      lg: "false",
                      md: "false",
                      sm: "false",
                      xl: "false",
                      xs: "auto",
                      zeroMinWidth: false
                    },
                    {
                      child: (
                        <Typography
                          align="inherit"
                          color="initial"
                          display="initial"
                          gutterBottom={false}
                          noWrap={true}
                          paragraph={false}
                          text="DOT_ADD_DATE"
                          variant="body1"
                        />
                      ),
                      lg: "false",
                      md: "false",
                      sm: "false",
                      xl: "false",
                      xs: "auto",
                      zeroMinWidth: false
                    },
                    {
                      child: (
                        <Typography
                          align="inherit"
                          color="initial"
                          display="initial"
                          gutterBottom={false}
                          noWrap={true}
                          paragraph={false}
                          text="PHONE "
                          variant="body1"
                        />
                      ),
                      lg: "false",
                      md: "false",
                      sm: "false",
                      xl: "false",
                      xs: "auto",
                      zeroMinWidth: false
                    },
                    {
                      child: (
                        <Typography
                          align="inherit"
                          color="initial"
                          display="initial"
                          gutterBottom={false}
                          noWrap={true}
                          paragraph={false}
                          text="EMAIL"
                          variant="body1"
                        />
                      ),
                      lg: "false",
                      md: "false",
                      sm: "false",
                      xl: "false",
                      xs: "auto",
                      zeroMinWidth: false
                    },
                    {
                      child: (
                        <Typography
                          align="inherit"
                          color="initial"
                          display="initial"
                          gutterBottom={false}
                          noWrap={false}
                          paragraph={false}
                          text="DELETE"
                          variant="body1"
                        />
                      ),
                      lg: "false",
                      md: "false",
                      sm: "false",
                      xl: "false",
                      xs: "auto",
                      zeroMinWidth: false
                    },
                    {
                      child: (
                        <div
                          style={{
                            width: "100%",
                            minHeight: "2em",
                            height: "100%",
                            borderRadius: "4px",
                            border: "1px dashed #dddddd"
                          }}
                        />
                      ),
                      lg: "false",
                      md: "false",
                      sm: "false",
                      xl: "false",
                      xs: "auto",
                      zeroMinWidth: false
                    },
                    {
                      child: (
                        <div
                          style={{
                            width: "100%",
                            minHeight: "2em",
                            height: "100%",
                            borderRadius: "4px",
                            border: "1px dashed #dddddd"
                          }}
                        />
                      ),
                      lg: "false",
                      md: "false",
                      sm: "false",
                      xl: "false",
                      xs: "auto",
                      zeroMinWidth: false
                    }
                  ]}
                  direction="row"
                  justify="flex-start"
                  spacing="2"
                  wrap="nowrap"
                />,
                <GridWithCells
                  key="item35"
                  alignContent="stretch"
                  alignItems="stretch"
                  cells={[
                    {
                      child: (
                        <div
                          style={{
                            width: "100%",
                            minHeight: "2em",
                            height: "100%",
                            borderRadius: "4px",
                            border: "1px dashed #dddddd"
                          }}
                        />
                      ),
                      lg: "false",
                      md: "false",
                      sm: "false",
                      xl: "false",
                      xs: "auto",
                      zeroMinWidth: false
                    },
                    {
                      child: (
                        <Typography
                          align="inherit"
                          color="initial"
                          display="initial"
                          gutterBottom={false}
                          noWrap={true}
                          paragraph={false}
                          text="3110220"
                          variant="body1"
                        />
                      ),
                      lg: "false",
                      md: "false",
                      sm: "false",
                      xl: "false",
                      xs: "auto",
                      zeroMinWidth: false
                    },
                    {
                      child: (
                        <Typography
                          align="inherit"
                          color="initial"
                          display="initial"
                          gutterBottom={false}
                          noWrap={true}
                          paragraph={false}
                          text="1063855"
                          variant="body1"
                        />
                      ),
                      lg: "false",
                      md: "false",
                      sm: "false",
                      xl: "false",
                      xs: "auto",
                      zeroMinWidth: false
                    },
                    {
                      child: (
                        <Typography
                          align="inherit"
                          color="initial"
                          display="initial"
                          gutterBottom={false}
                          noWrap={true}
                          paragraph={false}
                          text="HALANE TRANSPORTATION LLC"
                          variant="body1"
                        />
                      ),
                      lg: "false",
                      md: "false",
                      sm: "false",
                      xl: "false",
                      xs: "auto",
                      zeroMinWidth: false
                    },
                    {
                      child: (
                        <Typography
                          align="inherit"
                          color="initial"
                          display="initial"
                          gutterBottom={false}
                          noWrap={true}
                          paragraph={false}
                          text="COLUMBUS"
                          variant="body1"
                        />
                      ),
                      lg: "false",
                      md: "false",
                      sm: "false",
                      xl: "false",
                      xs: "auto",
                      zeroMinWidth: false
                    },
                    {
                      child: (
                        <Typography
                          align="inherit"
                          color="initial"
                          display="initial"
                          gutterBottom={false}
                          noWrap={false}
                          paragraph={false}
                          text="OH"
                          variant="body1"
                        />
                      ),
                      lg: "false",
                      md: "false",
                      sm: "false",
                      xl: "false",
                      xs: "auto",
                      zeroMinWidth: false
                    },
                    {
                      child: (
                        <Typography
                          align="inherit"
                          color="initial"
                          display="initial"
                          gutterBottom={false}
                          noWrap={true}
                          paragraph={false}
                          text="05-15-2020"
                          variant="body1"
                        />
                      ),
                      lg: "false",
                      md: "false",
                      sm: "false",
                      xl: "false",
                      xs: "auto",
                      zeroMinWidth: false
                    },
                    {
                      child: (
                        <Typography
                          align="inherit"
                          color="initial"
                          display="initial"
                          gutterBottom={false}
                          noWrap={true}
                          paragraph={false}
                          text="02-15-2020"
                          variant="body1"
                        />
                      ),
                      lg: "false",
                      md: "false",
                      sm: "false",
                      xl: "false",
                      xs: "auto",
                      zeroMinWidth: false
                    },
                    {
                      child: (
                        <Typography
                          align="inherit"
                          color="initial"
                          display="initial"
                          gutterBottom={false}
                          noWrap={true}
                          paragraph={false}
                          text="614-270-5094"
                          variant="body1"
                        />
                      ),
                      lg: "false",
                      md: "false",
                      sm: "false",
                      xl: "false",
                      xs: "auto",
                      zeroMinWidth: false
                    },
                    {
                      child: (
                        <Typography
                          align="inherit"
                          color="initial"
                          display="initial"
                          gutterBottom={false}
                          noWrap={false}
                          paragraph={false}
                          text="HALANETRANSPORTATION@GMAIL.COM"
                          variant="body1"
                        />
                      ),
                      lg: "false",
                      md: "false",
                      sm: "false",
                      xl: "false",
                      xs: "auto",
                      zeroMinWidth: false
                    },
                    {
                      child: <Button label="DELETE" variant="contained" />,
                      lg: "false",
                      md: "false",
                      sm: "false",
                      xl: "false",
                      xs: "auto",
                      zeroMinWidth: false
                    },
                    {
                      child: (
                        <div
                          style={{
                            width: "100%",
                            minHeight: "2em",
                            height: "100%",
                            borderRadius: "4px",
                            border: "1px dashed #dddddd"
                          }}
                        />
                      ),
                      lg: "false",
                      md: "false",
                      sm: "false",
                      xl: "false",
                      xs: "auto",
                      zeroMinWidth: false
                    }
                  ]}
                  direction="row"
                  justify="flex-start"
                  spacing="2"
                  wrap="nowrap"
                />
              ]}
              direction="column"
              spacing="2"
            />,
            <div
              key="item36"
              style={{
                width: "100%",
                minHeight: "2em",
                height: "100%",
                borderRadius: "4px",
                border: "1px dashed #dddddd"
              }}
            />,
            <Stack
              key="item37"
              cells={[
                <Typography
                  key="item38"
                  align="inherit"
                  color="initial"
                  display="initial"
                  gutterBottom={false}
                  noWrap={false}
                  paragraph={false}
                  text="POPPED QUERY"
                  variant="body1"
                />,
                <GridWithCells
                  key="item39"
                  alignContent="stretch"
                  alignItems="stretch"
                  cells={[
                    {
                      child: (
                        <div
                          style={{
                            width: "100%",
                            minHeight: "2em",
                            height: "100%",
                            borderRadius: "4px",
                            border: "1px dashed #dddddd"
                          }}
                        />
                      ),
                      lg: "false",
                      md: "false",
                      sm: "false",
                      xl: "false",
                      xs: "auto",
                      zeroMinWidth: false
                    },
                    {
                      child: (
                        <Typography
                          align="inherit"
                          color="initial"
                          display="initial"
                          gutterBottom={false}
                          noWrap={true}
                          paragraph={false}
                          text="DOT NUMBER"
                          variant="body1"
                        />
                      ),
                      lg: "false",
                      md: "false",
                      sm: "false",
                      xl: "false",
                      xs: "auto",
                      zeroMinWidth: false
                    },
                    {
                      child: (
                        <Typography
                          align="inherit"
                          color="initial"
                          display="initial"
                          gutterBottom={false}
                          noWrap={true}
                          paragraph={false}
                          text="MC_NUMBER"
                          variant="body1"
                        />
                      ),
                      lg: "false",
                      md: "false",
                      sm: "false",
                      xl: "false",
                      xs: "auto",
                      zeroMinWidth: false
                    },
                    {
                      child: (
                        <Typography
                          align="inherit"
                          color="initial"
                          display="initial"
                          gutterBottom={false}
                          noWrap={true}
                          paragraph={false}
                          text="LEGAL_NAME"
                          variant="body1"
                        />
                      ),
                      lg: "false",
                      md: "false",
                      sm: "false",
                      xl: "false",
                      xs: "auto",
                      zeroMinWidth: false
                    },
                    {
                      child: (
                        <Typography
                          align="inherit"
                          color="initial"
                          display="initial"
                          gutterBottom={false}
                          noWrap={true}
                          paragraph={false}
                          text="CITY"
                          variant="body1"
                        />
                      ),
                      lg: "false",
                      md: "false",
                      sm: "false",
                      xl: "false",
                      xs: "auto",
                      zeroMinWidth: false
                    },
                    {
                      child: (
                        <Typography
                          align="inherit"
                          color="initial"
                          display="initial"
                          gutterBottom={false}
                          noWrap={true}
                          paragraph={false}
                          text="STATE"
                          variant="body1"
                        />
                      ),
                      lg: "false",
                      md: "false",
                      sm: "false",
                      xl: "false",
                      xs: "auto",
                      zeroMinWidth: false
                    },
                    {
                      child: (
                        <Typography
                          align="inherit"
                          color="initial"
                          display="initial"
                          gutterBottom={false}
                          noWrap={true}
                          paragraph={false}
                          text="MC_ADD_DATE"
                          variant="body1"
                        />
                      ),
                      lg: "false",
                      md: "false",
                      sm: "false",
                      xl: "false",
                      xs: "auto",
                      zeroMinWidth: false
                    },
                    {
                      child: (
                        <Typography
                          align="inherit"
                          color="initial"
                          display="initial"
                          gutterBottom={false}
                          noWrap={false}
                          paragraph={false}
                          text="DOT_ADD_DATE"
                          variant="body1"
                        />
                      ),
                      lg: "false",
                      md: "false",
                      sm: "false",
                      xl: "false",
                      xs: "auto",
                      zeroMinWidth: false
                    },
                    {
                      child: (
                        <Typography
                          align="inherit"
                          color="initial"
                          display="initial"
                          gutterBottom={false}
                          noWrap={false}
                          paragraph={false}
                          text="PHONE"
                          variant="body1"
                        />
                      ),
                      lg: "false",
                      md: "false",
                      sm: "false",
                      xl: "false",
                      xs: "auto",
                      zeroMinWidth: false
                    },
                    {
                      child: (
                        <Typography
                          align="inherit"
                          color="initial"
                          display="initial"
                          gutterBottom={false}
                          noWrap={false}
                          paragraph={false}
                          text="EMAIL"
                          variant="body1"
                        />
                      ),
                      lg: "false",
                      md: "false",
                      sm: "false",
                      xl: "false",
                      xs: "auto",
                      zeroMinWidth: false
                    },
                    {
                      child: (
                        <Typography
                          align="inherit"
                          color="initial"
                          display="initial"
                          gutterBottom={false}
                          noWrap={false}
                          paragraph={false}
                          text="DELETE"
                          variant="body1"
                        />
                      ),
                      lg: "false",
                      md: "false",
                      sm: "false",
                      xl: "false",
                      xs: "auto",
                      zeroMinWidth: false
                    },
                    {
                      child: (
                        <div
                          style={{
                            width: "100%",
                            minHeight: "2em",
                            height: "100%",
                            borderRadius: "4px",
                            border: "1px dashed #dddddd"
                          }}
                        />
                      ),
                      lg: "false",
                      md: "false",
                      sm: "false",
                      xl: "false",
                      xs: "auto",
                      zeroMinWidth: false
                    }
                  ]}
                  direction="row"
                  justify="flex-start"
                  spacing="2"
                  wrap="nowrap"
                />,
                <GridWithCells
                  key="item40"
                  alignContent="stretch"
                  alignItems="stretch"
                  cells={[
                    {
                      child: (
                        <div
                          style={{
                            width: "100%",
                            minHeight: "2em",
                            height: "100%",
                            borderRadius: "4px",
                            border: "1px dashed #dddddd"
                          }}
                        />
                      ),
                      lg: "false",
                      md: "false",
                      sm: "false",
                      xl: "false",
                      xs: "auto",
                      zeroMinWidth: false
                    },
                    {
                      child: (
                        <Typography
                          align="inherit"
                          color="initial"
                          display="initial"
                          gutterBottom={false}
                          noWrap={true}
                          paragraph={false}
                          text="3110220"
                          variant="body1"
                        />
                      ),
                      lg: "false",
                      md: "false",
                      sm: "false",
                      xl: "false",
                      xs: "auto",
                      zeroMinWidth: false
                    },
                    {
                      child: (
                        <Typography
                          align="inherit"
                          color="initial"
                          display="initial"
                          gutterBottom={false}
                          noWrap={true}
                          paragraph={false}
                          text="1063855"
                          variant="body1"
                        />
                      ),
                      lg: "false",
                      md: "false",
                      sm: "false",
                      xl: "false",
                      xs: "auto",
                      zeroMinWidth: false
                    },
                    {
                      child: (
                        <Typography
                          align="inherit"
                          color="initial"
                          display="initial"
                          gutterBottom={false}
                          noWrap={true}
                          paragraph={false}
                          text="HALANE TRANSPORTATION LLC"
                          variant="body1"
                        />
                      ),
                      lg: "false",
                      md: "false",
                      sm: "false",
                      xl: "false",
                      xs: "auto",
                      zeroMinWidth: false
                    },
                    {
                      child: (
                        <Typography
                          align="inherit"
                          color="initial"
                          display="initial"
                          gutterBottom={false}
                          noWrap={true}
                          paragraph={false}
                          text="COLUMBUS"
                          variant="body1"
                        />
                      ),
                      lg: "false",
                      md: "false",
                      sm: "false",
                      xl: "false",
                      xs: "auto",
                      zeroMinWidth: false
                    },
                    {
                      child: (
                        <Typography
                          align="inherit"
                          color="initial"
                          display="initial"
                          gutterBottom={false}
                          noWrap={true}
                          paragraph={false}
                          text="OH"
                          variant="body1"
                        />
                      ),
                      lg: "false",
                      md: "false",
                      sm: "false",
                      xl: "false",
                      xs: "auto",
                      zeroMinWidth: false
                    },
                    {
                      child: (
                        <Typography
                          align="inherit"
                          color="initial"
                          display="initial"
                          gutterBottom={false}
                          noWrap={true}
                          paragraph={false}
                          text="05-15-2020"
                          variant="body1"
                        />
                      ),
                      lg: "false",
                      md: "false",
                      sm: "false",
                      xl: "false",
                      xs: "auto",
                      zeroMinWidth: false
                    },
                    {
                      child: (
                        <Typography
                          align="inherit"
                          color="initial"
                          display="initial"
                          gutterBottom={false}
                          noWrap={true}
                          paragraph={false}
                          text="02-15-2020"
                          variant="body1"
                        />
                      ),
                      lg: "false",
                      md: "false",
                      sm: "false",
                      xl: "false",
                      xs: "auto",
                      zeroMinWidth: false
                    },
                    {
                      child: (
                        <Typography
                          align="inherit"
                          color="initial"
                          display="initial"
                          gutterBottom={false}
                          noWrap={true}
                          paragraph={false}
                          text="614-270-5094"
                          variant="body1"
                        />
                      ),
                      lg: "false",
                      md: "false",
                      sm: "false",
                      xl: "false",
                      xs: "auto",
                      zeroMinWidth: false
                    },
                    {
                      child: (
                        <Typography
                          align="inherit"
                          color="initial"
                          display="initial"
                          gutterBottom={false}
                          noWrap={true}
                          paragraph={false}
                          text="HALANETRANSPORTATION@GMAIL.COM"
                          variant="body1"
                        />
                      ),
                      lg: "false",
                      md: "false",
                      sm: "false",
                      xl: "false",
                      xs: "auto",
                      zeroMinWidth: false
                    },
                    {
                      child: <Button label="Button" variant="contained" />,
                      lg: "false",
                      md: "false",
                      sm: "false",
                      xl: "false",
                      xs: "auto",
                      zeroMinWidth: false
                    },
                    {
                      child: (
                        <div
                          style={{
                            width: "100%",
                            minHeight: "2em",
                            height: "100%",
                            borderRadius: "4px",
                            border: "1px dashed #dddddd"
                          }}
                        />
                      ),
                      lg: "false",
                      md: "false",
                      sm: "false",
                      xl: "false",
                      xs: "auto",
                      zeroMinWidth: false
                    }
                  ]}
                  direction="row"
                  justify="flex-start"
                  spacing="2"
                  wrap="nowrap"
                />
              ]}
              direction="column"
              spacing="1"
            />,
            <div
              key="item41"
              style={{
                width: "100%",
                minHeight: "2em",
                height: "100%",
                borderRadius: "4px",
                border: "1px dashed #dddddd"
              }}
            />
          ]}
          direction="column"
          spacing="1"
        />,
        <div
          key="item42"
          style={{
            width: "100%",
            minHeight: "2em",
            height: "100%",
            borderRadius: "4px",
            border: "1px dashed #dddddd"
          }}
        />,
        <Stack
          key="item43"
          cells={[
            <Typography
              key="item44"
              align="inherit"
              color="initial"
              display="initial"
              gutterBottom={false}
              noWrap={false}
              paragraph={false}
              text="SAVED QUERIES"
              variant="body1"
            />,
            <GridWithCells
              key="item45"
              alignContent="stretch"
              alignItems="stretch"
              cells={[
                {
                  child: (
                    <div
                      style={{
                        width: "100%",
                        minHeight: "2em",
                        height: "100%",
                        borderRadius: "4px",
                        border: "1px dashed #dddddd"
                      }}
                    />
                  ),
                  lg: "false",
                  md: "false",
                  sm: "false",
                  xl: "false",
                  xs: "auto",
                  zeroMinWidth: false
                },
                {
                  child: (
                    <Typography
                      align="inherit"
                      color="initial"
                      display="initial"
                      gutterBottom={false}
                      noWrap={true}
                      paragraph={false}
                      text="NAME OF SAVE"
                      variant="body1"
                    />
                  ),
                  lg: "false",
                  md: "false",
                  sm: "false",
                  xl: "false",
                  xs: "auto",
                  zeroMinWidth: false
                },
                {
                  child: (
                    <Typography
                      align="inherit"
                      color="initial"
                      display="initial"
                      gutterBottom={false}
                      noWrap={true}
                      paragraph={false}
                      text="DATE QUERIED"
                      variant="body1"
                    />
                  ),
                  lg: "false",
                  md: "false",
                  sm: "false",
                  xl: "false",
                  xs: "auto",
                  zeroMinWidth: false
                },
                {
                  child: (
                    <Typography
                      align="inherit"
                      color="initial"
                      display="initial"
                      gutterBottom={false}
                      noWrap={true}
                      paragraph={false}
                      text="QUERIED COUNT"
                      variant="body1"
                    />
                  ),
                  lg: "false",
                  md: "false",
                  sm: "false",
                  xl: "false",
                  xs: "auto",
                  zeroMinWidth: false
                },
                {
                  child: (
                    <Typography
                      align="inherit"
                      color="initial"
                      display="initial"
                      gutterBottom={false}
                      noWrap={true}
                      paragraph={false}
                      text="POPPED COUNT"
                      variant="body1"
                    />
                  ),
                  lg: "false",
                  md: "false",
                  sm: "false",
                  xl: "false",
                  xs: "auto",
                  zeroMinWidth: false
                },
                {
                  child: (
                    <Typography
                      align="inherit"
                      color="initial"
                      display="initial"
                      gutterBottom={false}
                      noWrap={true}
                      paragraph={false}
                      text="VIEW IN DETAIL"
                      variant="body1"
                    />
                  ),
                  lg: "false",
                  md: "false",
                  sm: "false",
                  xl: "false",
                  xs: "auto",
                  zeroMinWidth: false
                },
                {
                  child: (
                    <Typography
                      align="inherit"
                      color="initial"
                      display="initial"
                      gutterBottom={false}
                      noWrap={true}
                      paragraph={false}
                      text="DOWNLOAD QUERIED"
                      variant="body1"
                    />
                  ),
                  lg: "false",
                  md: "false",
                  sm: "false",
                  xl: "false",
                  xs: "auto",
                  zeroMinWidth: false
                },
                {
                  child: (
                    <Typography
                      align="inherit"
                      color="initial"
                      display="initial"
                      gutterBottom={false}
                      noWrap={true}
                      paragraph={false}
                      text="DOWNLOAD POPPED"
                      variant="body1"
                    />
                  ),
                  lg: "false",
                  md: "false",
                  sm: "false",
                  xl: "false",
                  xs: "auto",
                  zeroMinWidth: false
                },
                {
                  child: (
                    <Typography
                      align="inherit"
                      color="initial"
                      display="initial"
                      gutterBottom={false}
                      noWrap={false}
                      paragraph={false}
                      text="DELETE "
                      variant="body1"
                    />
                  ),
                  lg: "false",
                  md: "false",
                  sm: "false",
                  xl: "false",
                  xs: "auto",
                  zeroMinWidth: false
                },
                {
                  child: (
                    <div
                      style={{
                        width: "100%",
                        minHeight: "2em",
                        height: "100%",
                        borderRadius: "4px",
                        border: "1px dashed #dddddd"
                      }}
                    />
                  ),
                  lg: "false",
                  md: "false",
                  sm: "false",
                  xl: "false",
                  xs: "auto",
                  zeroMinWidth: false
                }
              ]}
              direction="row"
              justify="flex-start"
              spacing="2"
              wrap="nowrap"
            />,
            <GridWithCells
              key="item46"
              alignContent="stretch"
              alignItems="stretch"
              cells={[
                {
                  child: (
                    <div
                      style={{
                        width: "100%",
                        minHeight: "2em",
                        height: "100%",
                        borderRadius: "4px",
                        border: "1px dashed #dddddd"
                      }}
                    />
                  ),
                  lg: "false",
                  md: "false",
                  sm: "false",
                  xl: "false",
                  xs: "auto",
                  zeroMinWidth: false
                },
                {
                  child: (
                    <Typography
                      align="inherit"
                      color="initial"
                      display="initial"
                      gutterBottom={false}
                      noWrap={true}
                      paragraph={false}
                      text="2020SEP2020TO2020"
                      variant="body1"
                    />
                  ),
                  lg: "false",
                  md: "false",
                  sm: "false",
                  xl: "false",
                  xs: "auto",
                  zeroMinWidth: false
                }
              ]}
              direction="row"
              justify="flex-start"
              spacing="1"
              wrap="nowrap"
            />
          ]}
          direction="column"
          spacing="1"
        />,
        <div
          key="item47"
          style={{
            width: "100%",
            minHeight: "2em",
            height: "100%",
            borderRadius: "4px",
            border: "1px dashed #dddddd"
          }}
        />,
        <div
          key="item48"
          style={{
            width: "100%",
            minHeight: "2em",
            height: "100%",
            borderRadius: "4px",
            border: "1px dashed #dddddd"
          }}
        />,
        <div
          key="item49"
          style={{
            width: "100%",
            minHeight: "2em",
            height: "100%",
            borderRadius: "4px",
            border: "1px dashed #dddddd"
          }}
        />
      ]}
      direction="column"
      justifyItems="stretch"
      spacing="2"
    />
  );
}

export default NewComponent;
