import React from "react";
import Stack from "usr/layout/Stack.comp";
import Typography from "usr/data-display/Typography.comp";
import GridWithCells from "usr/layout/GridWithCells.comp";
import TextField from "usr/inputs/TextField.comp";
import NumberField from "usr/inputs/NumberField.comp";
import DateTimePicker from "usr/inputs/DateTimePicker.comp";
import Checkbox from "usr/inputs/Checkbox.comp";
import SelectWithOptions from "usr/inputs/SelectWithOptions.comp";

export interface NewComponentProps {}

function NewComponent(props: NewComponentProps): JSX.Element {
  return (
    <Stack
      cells={[
        <Typography
          key="item1"
          align="inherit"
          color="initial"
          display="initial"
          gutterBottom={false}
          noWrap={false}
          paragraph={false}
          text="MC Grants Date Query"
          variant="body1"
        />,
        <div
          key="item2"
          style={{
            width: "100%",
            minHeight: "2em",
            height: "100%",
            borderRadius: "4px",
            border: "1px dashed #dddddd"
          }}
        />,
        <GridWithCells
          key="item3"
          alignContent="stretch"
          alignItems="stretch"
          cells={[
            {
              child: (
                <Stack
                  cells={[
                    <Typography
                      key="item4"
                      align="inherit"
                      color="initial"
                      display="initial"
                      gutterBottom={false}
                      noWrap={false}
                      paragraph={false}
                      text="States"
                      variant="body1"
                    />,
                    <TextField
                      key="item5"
                      debounceDelay={300}
                      formControl={{ margin: "none" }}
                      helperText="States here"
                      inputControl={{ type: "text" }}
                    />,
                    <Typography
                      key="item6"
                      align="inherit"
                      color="initial"
                      display="initial"
                      gutterBottom={false}
                      noWrap={false}
                      paragraph={false}
                      text="Typed states will show here {}"
                      variant="body1"
                    />,
                    <Typography
                      key="item7"
                      align="inherit"
                      color="initial"
                      display="initial"
                      gutterBottom={false}
                      noWrap={false}
                      paragraph={false}
                      text="ZipCodes"
                      variant="body1"
                    />,
                    <NumberField
                      key="item8"
                      debounceDelay={300}
                      formControl={{ margin: "none" }}
                    />,
                    <Typography
                      key="item9"
                      align="inherit"
                      color="initial"
                      display="initial"
                      gutterBottom={false}
                      noWrap={false}
                      paragraph={false}
                      text="Typed Zipcodes Show here{}"
                      variant="body1"
                    />,
                    <div
                      key="item10"
                      style={{
                        width: "100%",
                        minHeight: "2em",
                        height: "100%",
                        borderRadius: "4px",
                        border: "1px dashed #dddddd"
                      }}
                    />
                  ]}
                  direction="column"
                  spacing="1"
                />
              ),
              lg: "false",
              md: "false",
              sm: "false",
              xl: "false",
              xs: "auto",
              zeroMinWidth: false
            },
            {
              child: (
                <Stack
                  cells={[
                    <Typography
                      key="item11"
                      align="inherit"
                      color="initial"
                      display="initial"
                      gutterBottom={false}
                      noWrap={false}
                      paragraph={false}
                      text="Dates between"
                      variant="body1"
                    />,
                    <Typography
                      key="item12"
                      align="inherit"
                      color="initial"
                      display="initial"
                      gutterBottom={false}
                      noWrap={false}
                      paragraph={false}
                      text="From Date"
                      variant="body1"
                    />,
                    <DateTimePicker key="item13" variant="dialog" />,
                    <Typography
                      key="item14"
                      align="inherit"
                      color="initial"
                      display="initial"
                      gutterBottom={false}
                      noWrap={false}
                      paragraph={false}
                      text="End Date"
                      variant="body1"
                    />,
                    <DateTimePicker key="item15" variant="dialog" />,
                    <div
                      key="item16"
                      style={{
                        width: "100%",
                        minHeight: "2em",
                        height: "100%",
                        borderRadius: "4px",
                        border: "1px dashed #dddddd"
                      }}
                    />,
                    <div
                      key="item17"
                      style={{
                        width: "100%",
                        minHeight: "2em",
                        height: "100%",
                        borderRadius: "4px",
                        border: "1px dashed #dddddd"
                      }}
                    />
                  ]}
                  direction="column"
                  spacing="1"
                />
              ),
              lg: "false",
              md: "false",
              sm: "false", 
              xl: "false",
              xs: "auto",
              zeroMinWidth: false
            },
            {
              child: (
                <Stack
                  cells={[
                    <Typography
                      key="item18"
                      align="inherit"
                      color="initial"
                      display="initial"
                      gutterBottom={false}
                      noWrap={false}
                      paragraph={false}
                      text="Order By"
                      variant="body1"
                    />,
                    <GridWithCells
                      key="item19"
                      alignContent="stretch"
                      alignItems="stretch"
                      direction="row"
                      justify="flex-start"
                      spacing="0"
                      wrap="nowrap"
                    />,
                    <GridWithCells
                      key="item20"
                      alignContent="stretch"
                      alignItems="stretch"
                      cells={[
                        {
                          child: (
                            <Checkbox
                              color="default"
                              label="City Name"
                              labelPlacement="end"
                            />
                          ),
                          lg: "false",
                          md: "false",
                          sm: "false",
                          xl: "false",
                          xs: "auto",
                          zeroMinWidth: false
                        },
                        {
                          child: (
                            <SelectWithOptions
                              formControl={{
                                color: "primary",
                                fullWidth: true,
                                margin: "none",
                                variant: "standard"
                              }}
                              options={[
                                {
                                  disabled: false,
                                  id: "0000",
                                  label: "Ascending",
                                  value: "asc"
                                },
                                {
                                  id: "0001",
                                  label: "descending",
                                  value: "des"
                                }
                              ]}
                              selectedValue="asc"
                            />
                          ),
                          lg: "false",
                          md: "false",
                          sm: "false",
                          xl: "false",
                          xs: "auto",
                          zeroMinWidth: false
                        }
                      ]}
                      direction="row"
                      justify="flex-start"
                      spacing="0"
                      wrap="nowrap"
                    />,
                    <GridWithCells
                      key="item21"
                      alignContent="stretch"
                      alignItems="stretch"
                      cells={[
                        {
                          child: <Checkbox color="default" label="Date" />,
                          lg: "false",
                          md: "false",
                          sm: "false",
                          xl: "false",
                          xs: "auto",
                          zeroMinWidth: false
                        },
                        {
                          child: (
                            <SelectWithOptions
                              formControl={{
                                color: "primary",
                                fullWidth: true,
                                margin: "none",
                                variant: "standard"
                              }}
                              options={[
                                {
                                  id: "0000",
                                  label: "Ascending",
                                  value: "asc"
                                },
                                {
                                  id: "0001",
                                  label: "Descending",
                                  value: "des"
                                }
                              ]}
                              selectedValue="asc"
                            />
                          ),
                          lg: "false",
                          md: "false",
                          sm: "false",
                          xl: "false",
                          xs: "auto",
                          zeroMinWidth: false
                        }
                      ]}
                      direction="row"
                      justify="flex-start"
                      spacing="0"
                      wrap="nowrap"
                    />,
                    <div
                      key="item22"
                      style={{
                        width: "100%",
                        minHeight: "2em",
                        height: "100%",
                        borderRadius: "4px",
                        border: "1px dashed #dddddd"
                      }}
                    />
                  ]}
                  direction="column"
                  spacing="1"
                />
              ),
              lg: "false",
              md: "false",
              sm: "false",
              xl: "false",
              xs: "auto",
              zeroMinWidth: false
            }
          ]}
          direction="row"
          justify="flex-start"
          spacing="0"
          wrap="nowrap"
        />,
        <div
          key="item23"
          style={{
            width: "100%",
            minHeight: "2em",
            height: "100%",
            borderRadius: "4px",
            border: "1px dashed #dddddd"
          }}
        />,
        <GridWithCells
          key="item24"
          alignContent="stretch"
          alignItems="stretch"
          cells={[
            {
              child: (
                <div
                  style={{
                    width: "100%",
                    minHeight: "2em",
                    height: "100%",
                    borderRadius: "4px",
                    border: "1px dashed #dddddd"
                  }}
                />
              ),
              lg: "false",
              md: "false",
              sm: "false",
              xl: "false",
              xs: "auto",
              zeroMinWidth: false
            },
            {
              child: (
                <div
                  style={{
                    width: "100%",
                    minHeight: "2em",
                    height: "100%",
                    borderRadius: "4px",
                    border: "1px dashed #dddddd"
                  }}
                />
              ),
              lg: "false",
              md: "false",
              sm: "false",
              xl: "false",
              xs: "auto",
              zeroMinWidth: false
            },
            {
              child: (
                <div
                  style={{
                    width: "100%",
                    minHeight: "2em",
                    height: "100%",
                    borderRadius: "4px",
                    border: "1px dashed #dddddd"
                  }}
                />
              ),
              lg: "false",
              md: "false",
              sm: "false",
              xl: "false",
              xs: "auto",
              zeroMinWidth: false
            },
            {
              child: (
                <div
                  style={{
                    width: "100%",
                    minHeight: "2em",
                    height: "100%",
                    borderRadius: "4px",
                    border: "1px dashed #dddddd"
                  }}
                />
              ),
              lg: "false",
              md: "false",
              sm: "false",
              xl: "false",
              xs: "auto",
              zeroMinWidth: false
            },
            {
              child: (
                <div
                  style={{
                    width: "100%",
                    minHeight: "2em",
                    height: "100%",
                    borderRadius: "4px",
                    border: "1px dashed #dddddd"
                  }}
                />
              ),
              lg: "false",
              md: "false",
              sm: "false",
              xl: "false",
              xs: "auto",
              zeroMinWidth: false
            }
          ]}
          direction="row"
          justify="flex-start"
          spacing="0"
          wrap="nowrap"
        />,
        <GridWithCells
          key="item25"
          alignContent="stretch"
          alignItems="stretch"
          cells={[
            {
              child: (
                <div
                  style={{
                    width: "100%",
                    minHeight: "2em",
                    height: "100%",
                    borderRadius: "4px",
                    border: "1px dashed #dddddd"
                  }}
                />
              ),
              lg: "false",
              md: "false",
              sm: "false",
              xl: "false",
              xs: "auto",
              zeroMinWidth: false
            },
            {
              child: (
                <div
                  style={{
                    width: "100%",
                    minHeight: "2em",
                    height: "100%",
                    borderRadius: "4px",
                    border: "1px dashed #dddddd"
                  }}
                />
              ),
              lg: "false",
              md: "false",
              sm: "false",
              xl: "false",
              xs: "auto",
              zeroMinWidth: false
            },
            {
              child: (
                <div
                  style={{
                    width: "100%",
                    minHeight: "2em",
                    height: "100%",
                    borderRadius: "4px",
                    border: "1px dashed #dddddd"
                  }}
                />
              ),
              lg: "false",
              md: "false",
              sm: "false",
              xl: "false",
              xs: "auto",
              zeroMinWidth: false
            }
          ]}
          direction="row"
          justify="flex-start"
          spacing="0"
          wrap="nowrap"
        />,
        <Stack
          key="item26"
          cells={[
            <GridWithCells
              key="item27"
              alignContent="stretch"
              alignItems="stretch"
              cells={[
                {
                  child: (
                    <div
                      style={{
                        width: "100%",
                        minHeight: "2em",
                        height: "100%",
                        borderRadius: "4px",
                        border: "1px dashed #dddddd"
                      }}
                    />
                  ),
                  lg: "false",
                  md: "false",
                  sm: "false",
                  xl: "false",
                  xs: "auto",
                  zeroMinWidth: false
                },
                {
                  child: (
                    <div
                      style={{
                        width: "100%",
                        minHeight: "2em",
                        height: "100%",
                        borderRadius: "4px",
                        border: "1px dashed #dddddd"
                      }}
                    />
                  ),
                  lg: "false",
                  md: "false",
                  sm: "false",
                  xl: "false",
                  xs: "auto",
                  zeroMinWidth: false
                },
                {
                  child: (
                    <div
                      style={{
                        width: "100%",
                        minHeight: "2em",
                        height: "100%",
                        borderRadius: "4px",
                        border: "1px dashed #dddddd"
                      }}
                    />
                  ),
                  lg: "false",
                  md: "false",
                  sm: "false",
                  xl: "false",
                  xs: "auto",
                  zeroMinWidth: false
                }
              ]}
              direction="row"
              justify="flex-start"
              spacing="0"
              wrap="nowrap"
            />,
            <div
              key="item28"
              style={{
                width: "100%",
                minHeight: "2em",
                height: "100%",
                borderRadius: "4px",
                border: "1px dashed #dddddd"
              }}
            />,
            <div
              key="item29"
              style={{
                width: "100%",
                minHeight: "2em",
                height: "100%",
                borderRadius: "4px",
                border: "1px dashed #dddddd"
              }}
            />
          ]}
          direction="column"
          spacing="1"
        />,
        <div
          key="item30"
          style={{
            width: "100%",
            minHeight: "2em",
            height: "100%",
            borderRadius: "4px",
            border: "1px dashed #dddddd"
          }}
        />,
        <div
          key="item31"
          style={{
            width: "100%",
            minHeight: "2em",
            height: "100%",
            borderRadius: "4px",
            border: "1px dashed #dddddd"
          }}
        />,
        <div
          key="item32"
          style={{
            width: "100%",
            minHeight: "2em",
            height: "100%",
            borderRadius: "4px",
            border: "1px dashed #dddddd"
          }}
        />,
        <div
          key="item33"
          style={{
            width: "100%",
            minHeight: "2em",
            height: "100%",
            borderRadius: "4px",
            border: "1px dashed #dddddd"
          }}
        />,
        <div
          key="item34"
          style={{
            width: "100%",
            minHeight: "2em",
            height: "100%",
            borderRadius: "4px",
            border: "1px dashed #dddddd"
          }}
        />
      ]}
      direction="column"
      spacing="1"
    />
  );
}

export default NewComponent;
